#!/usr/bin/env node
/**
 * Compages CLI
 */
'use strict';
const chalk   = require('chalk');
const path    = require('path');
const fs      = require('fs');
const Command = require('commander').Command;

let program = new Command('cpg');

program
	.version('0.1.0');

let cpg = require('../src')();

require('./setup')(program, cpg);
require('./site')(program, cpg);
require('./serve')(program, cpg);
require('./build')(program, cpg);

program.parse(process.argv);

/**
 * Error handling, helps track down unhandled errors.
 */
process.on('unhandledRejection', (reason, p) => 
{
	console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});

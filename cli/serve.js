/** 
 * Runs the site.
 */
const debug = require('debug')('cpg:cli:serve');
const http  = require('http');

module.exports = (program, cpg) =>
{
	debug('setting up');
	let command = program
		.command('serve [port/socket]')
		.alias('s')
		.description('Serves the project via HTTP or local socket.');

	command.action(action(command,cpg));

	return command;
};

const action = (command, cpg) => port =>
{
	if (!port) 
		port = 3000;

	let app = cpg.serve();

	let srvr = http.Server(app);

	srvr.listen(port);
};

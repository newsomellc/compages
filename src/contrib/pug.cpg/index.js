'use strict';
const debug = require('debug')('cpg:pug');
module.exports = (cpg, p) =>
{
	debug('instantiating'); 
	const self = {compages:true};

	self.serve = () =>
	{
		return (req, res, next) =>
		{
			debug('made it this far.');
			res.render(req.template||'index', {});
		};
	};

	self.build = () =>
	{

	};

	self.urlPattern = () => p.url_pattern;

	return self;
};

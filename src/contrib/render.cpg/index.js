'use strict';
const debug = require('debug')('cpg:render');

module.exports = (cpg, p) =>
{
	debug('instantiating'); 
	const self = {compages:true};

	self.serve = () =>
	{
		return (res, req, next) =>
		{
			//get the file at the source
			//pass it to the renderer
			//pipe rendering output to client
			res.send('test');
		};
	};

	self.build = () =>
	{
		//figure out a graceful way to get the renderer passed to here.
	};
	
	self.urlPattern = () => p.url_pattern;

	return self;
};

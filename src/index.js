/** 
 * Compages -- main.
 */
'use strict';
const debug = require('debug')('cpg:core');

const getconfig = require('./get-cpg-config');
const Site      = require('./site');

module.exports = (conf) =>
{
	const cpg = {};

	const sites = Object.create(null);

	if (conf)
	{
		if (typeof conf === 'string')
			conf = getconfig(conf).get();
	}
	else
	{
		conf = getconfig().get();
	}

	cpg.contrib = module.exports.contrib;

	cpg.use = (site) =>
	{
		sites[site.id] = site;
	};

	cpg.serve = () =>
	{
		let app = require('express')();

		Object.keys(sites).forEach(id => 
		{
			app.use(sites[id].serve());
		});
		if (process.env.NODE_ENV === 'development')
			app.use(require('morgan')('dev'));
		return app;
	};

	Object.keys(conf).forEach(id =>
	{
		debug('Setting up site:', id);
		let prms = conf[id];
		if (prms.app)
			cpg.use(cpg.contrib(cpg, prms));
		else
			cpg.use(Site(cpg, prms));
	});
	return cpg;
};

module.exports.contrib = new Proxy(Object.create(null), 
{
	get : (target, name) =>
	{
		let join = require('path').join;
		name = name.replace('_', '-');
		return require(join(__dirname, 'contrib', name));
	}
});

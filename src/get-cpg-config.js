/**
 * Gets the ILS config as an nconf object.
 */
'use strict';
const file_exists = require('fs').existsSync;
const pjoin       = require('path').join;
const NConf       = require('nconf').Provider;
const debug       = require('debug')('compages:conf:getconfig');
const chalk       = require('chalk');

module.exports = p =>
{
	//firstly, get the paths to our (possible) config files.
	let cpgfile, env, cwd;

	if (typeof p === 'string')
	{
		cpgfile = p;
		p  = Object.create(null);
	}
	if (!p)
		p = Object.create(null);

	cpgfile = p.cpgfile || cpgfile;
	cwd     = p.cwd     || process.cwd();
	env     = p.env     || process.env.NODE_ENV||'development';

	let conf = new NConf();
	//conf.argv();
	//conf.env();

	let project_env_file  = pjoin(cwd, `compages.${env}.json`);
	let project_base_file = pjoin(cwd, 'compages.json');

	//Then, upon checking if they exist, add them as config options.
	if (cpgfile)
	{
		debug('trying to load config file:', cpgfile);
		if (cpgfile && file_exists(cpgfile))
			conf.file('cpgfile', cpgfile);
		else
			debug(cpgfile, 'doesn\'t exist');
	}

	debug('trying to load config file:', project_env_file);
	if (file_exists(project_env_file))
		conf.file('project-env', project_env_file);
	else
		debug(project_env_file, 'doesn\'t exist');

	debug('trying to load config file:', project_base_file);
	if (file_exists(project_base_file))
		conf.file('project-base', project_base_file);
	else
		debug(project_base_file, 'doesn\'t exist');

	if (!conf.get())
		throw Error(chalk.cyan(`Compages is not configured or no config loaded. Type ${chalk.yellow('cpg setup')} to get started.`));

	return conf;
}

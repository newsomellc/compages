const chalk    = require('chalk');

const Chainify = require('./Chainify');
/**
 *  CLI output system.
 */
module.exports = params =>
{
	if (!params)
		params = Object.create(null);

	if (typeof params === 'function')
	{
		var stdout = params;
		var stderr = params;
	}
	else
	{
		var stdout = params.stdout || ((text) => process.stdout.write(text));
		var stderr = params.stderr || ((text) => process.stderr.write(text));
	}

	let out = function out_process (...vals)
	{
		let text = vals.join(this.get('separator'));

		if (this.get('center'))
			text = center_text(text);

		if (this.get('newline'))
			text += '\n';

		if (this.get('wrap'))
			text = wrap_text(text);

		switch(this.get('dest'))
		{
			case 'stderr':
				return stderr(text);
			case 'stdout':
				return stdout(text);
			case 'variable':
				return this.return(text);
		}
	};

	let self = Chainify(Object.create(PROTOTYPE), out, CHAINABLE_METHODS);
	return self;
};

/**
 * Makes this chain write to stdout.
 */ 
const stdout = function (...vals)
{
	this.set('dest', 'stdout');
	this.next(...vals);
}

/**
 * Makes this write to stderr.
 */ 
const stderr = function (...vals)
{
	this.set('dest', 'stderr');
	this.next(...vals);
}

/**
 * Sets text to be centered when finally output.
 */
const center = function (...vals)
{
	this.set('center', true);
	this.next(...vals);
}

/**
 * Sets the separator.  Returns the current dispatch function, rather than replacing it.
 */
const separator = function (separator)
{
	this.set('separator', separator);
	this.unchain();
}

/**
 * Turns wrapping off.  We do our own wrapping in order to break on words. This disables that if needed.
 */
const noWrap = function (...vals)
{
	this.set('wrap', false);
	//sorry, for now can't paginate if wrap is off.
	this.set('paginate',false);
	this.next(...vals);
};

/**
 * Turns automatic adding of newlines at the end of each call.
 */
const noNL = function (...vals)
{
	this.set('newline', false);
	this.next(...vals);
};

/**
 * Signals dispatch to add pagination to output.
 */
const paginate = function (...vals)
{
	 //sorry, for now can't paginate if wrap is off.
	this.set('wrap',  true);
	this.set('paginate',  true);
	this.next(...vals);
};

/**
 * Just gets the formatted text, skipping the output step.
 */
const get = function (...vals)
{
	this.set('dest', 'variable');
	this.set('newline', false);
	this.next(...vals);
};

/**
 * Pad text so it's centered in the console.
 * Multi-line text will cause it to center relative to the longest line.
 */
const center_text = (text, cols=process.stdout.columns) =>
{
	let a = text.split('\n');
	if (a.length > 1)
	{
		let mlen = 0;

		mlen = a.reduce((num,line) => Math.max(num,stripvt100(line).length), mlen);

		if (mlen > process.stdout.columns)
			return text;

		let padding = Array(Math.ceil((cols - mlen)/2) + 1).join(' ');

		return a.reduce((carry, current) => 
		{
			return carry + padding + current + '\n';
		}, '');

	}
	else
	{
		if (stripvt100(text).length > process.stdout.columns)
			return text;
		else
			return Array(Math.ceil((cols - stripvt100(text).length)/2) + 1).join(' ') + text;
	}
};

/**
 * After formatting of text is done, this wraps the text, breaking on words, or at the end of columns.
 * TODO: update to handle VT100 codes, find a better unified way.
 */
const wrap_text = (text, cols=process.stdout.columns) =>
{
	cols = cols || process.stdout.columns;

	if (text.length < cols)
		return text;

	broken_lines = text.match(new RegExp(`([^\\n]{0,${cols}}[ \\n-]?)`, 'g'));
	return broken_lines.join('\n').trim();
};

/**
 * Strips out vt100 codes, so they don't offset measurements used when centering
 */
const stripvt100 = str => str.replace(/\\u001B\[[\d;]*[^\d;]/g, '');

const CHAINABLE_METHODS = 
{
	stdout    : stdout,
	stderr    : stderr,
	center    : center,
	separator : separator,
	noWrap    : noWrap,
	noNL      : noNL,
	paginate  : paginate,
	get       : get,
};

/**
 * Sets up all the chalk methods.
 */
const chalk_call = key => function chalk_call (...vals)
{
	vals = vals.map(str=>chalk[key](str));
	this.next(...vals);
};

['reset','bold','dim','italic','underline','inverse','hidden','strikethrough','visible','black','red','green','yellow',
'blue','magenta','cyan','white','gray','redBright','greenBright','yellowBright','blueBright','magentaBright',
'cyanBright','whiteBright','bgBlack','bgRed','bgGreen','bgYellow','bgBlue','bgMagenta','bgCyan','bgWhite',
'bgBlackBright','bgRedBright','bgGreenBright','bgYellowBright','bgBlueBright','bgMagentaBright','bgCyanBright',
'bgWhiteBright'].forEach(k => CHAINABLE_METHODS[k] = chalk_call(k));

const PROTOTYPE =
{
	dest      : 'stderr',
	wrap      : false,
	paginate  : false,
	separator : ' ',
	center    : false,
	newline   : true,
};

module.exports.PROTOTYPE = PROTOTYPE;

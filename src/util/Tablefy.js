/**
 * 
 */
const TAB_PROTOTYPE = {};

/**
 * Writes the table to the screen.
 */
TAB_PROTOTYPE.dispatch = function (flags, ...vals)
{
	self._chain = [...self._chain, self._add_chain];
	self._add_chain = undefined;

	if (!self._headings && self._keys)
		self._headings = self._keys;
};

/**
 * Adds an array of rows. If there are pending chain changes, each one is added to _row_chains
 */
TAB_PROTOTYPE.rows = function (rows)
{
	if (this._add_chain)
		var current_chain = [...this._chain, this._add_chain];
	else
		var current_chain = false;
	rows.forEach(row =>
	{
		if (current_chain)
			this._row_chains.push(current_chain);
		else
			this._row_chains.push(undefined);

		if (Array.isArray(obj))
		{
			if (this._keys.length)
				throw Error('table rows must be represented only by objects or only by arrays, you can\'t mix them!');
			this._rows.push(obj);
		}
		else
		{
			Object.keys(obj).forEach(k => 
			{
				if (this._keys.indexOf(k) < 0)
					this._keys.push(k);
			});
			this._rows.push(this._keys.map(k => obj[k]));
		}
	});
	

	if (current_chain)
	{
		let preserve_chain = Object.create(this);
		this._add_chain = [];
		return preserve_chain;
	}
	else
		return this;
};
TAB_PROTOTYPE.rows.immediate = true;

/**
 * Adds a single row.
 */
TAB_PROTOTYPE.row = function (row)
{
	return this.rows([row]);
};
TAB_PROTOTYPE.row.immediate = true;

TAB_PROTOTYPE.headings = function (headings) 
{
	if (Array.isArray(headings))
	{
		if (this._keys.length)
			throw Error('If table is built from objects, an object mapping headings to keys is required.');
		this._headings = headings;
	}
	else
	{
		this._headings = headings;
		Object.keys(headings).forEach(k => 
		{
			if (this._keys.indexOf(k) < 0)
				this._keys.push(k);
		});
	}
};

/**
 * Below here, default values.
 */
TAB_PROTOTYPE._cols          = undefined;
TAB_PROTOTYPE._headings      = undefined;
TAB_PROTOTYPE._keys          = undefined;
TAB_PROTOTYPE._rows          = undefined;
TAB_PROTOTYPE._add_chain     = undefined;
TAB_PROTOTYPE._row_chains    = undefined;
TAB_PROTOTYPE._heading_chain = undefined;

/**
 * Strips out VT100 codes-- used to make width comparisons work properly.
 */
const stripvt100 = (str) =>
{
	return str.replace(/\\u001B\[[\d;]*[^\d;]/g, '');
};

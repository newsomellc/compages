'use strict';
/**
 * A (hopefully) simple and elegant solution to the problem of setting up 
 * method chains.
 */
const Chainify = module.exports = (base, callback, methods, chain=[]) =>
{
	if (!methods)
		methods = base;

	if (Array.isArray(methods))
		methods = methods.reduce((methods, key) =>
		{
			methods[key] = base[key];
			return methods;
		}, Object.create(null));

	chain = [callback || console.log, ...chain];

	/**
	 * This function becomes the principle target of the proxy-- whenever you 
	 * call any link on your chain, this is the function that's actually being
	 * called.
	 */
	const dispatch = (...vals) =>
	{
		let current = 0;
		let override_return;

		/**
		 * Context object-- is passed to your chainable methods via the `this`
		 * parameter. So yes, your chainable methods must be non-arrow 
		 * functions.
		 */
		let ctx = Object.create(null);

		/**
		 * Calls the next function in your chain.
		 */
		ctx.next = (...vals) => chain[current++].call(ctx, ...vals);

		/**
		 * Sets a value on the base object.
		 */
		ctx.set = (key, val) => base[key] = val;

		/**
		 * Gets a value from the base object, bypassing the proxy.
		 */
		ctx.get = (key) => base[key];

		/**
		 * Calls a function from the base object with the given params.
		 */
		ctx.call = (key, ...vals) => base[key](...vals);

		/**
		 * Removes the current function from the chain.
		 */
		ctx.unchain = () => chain.splice(current-1, 1);

		/**
		 * If you want to return something other than the proxy for further 
		 * chaining.
		 */
		ctx.return = (value) => override_return = value;

		ctx.next(...vals);

		if (override_return)
			return override_return;
		else
			return prox;
	};

	/**
	 * Proxy getter.
	 */
	const getter = (target, name) =>
	{
		if (methods[name])
			return Chainify(Object.create(base), methods[name], methods, chain);
		else 
			return base[name];
	};

	let prox = new Proxy(dispatch, {get : getter});
	return prox;
};

/**
 * Site framework.
 */
'use strict';
const VHost = require('vhost');
const debug = require('debug')('cpg:site');
module.exports = (cpg, p) =>
{
	debug('instantiating:', p.id);
	const self = {compages:true};
	/**
	 * Just like express, attach something to this app.
	 * Unlike express, since this is only for static sites, there's no .get .post
	 * etc. But there might be once I add fallback routing.
	 */
	self.use = (path, app) =>
	{
		if (typeof path === 'function')
		{
			app = path;
			path = /.*/;
		}

		if (path && path.compages)
		{
			app = path;
			path = null;
		}

		if (!app.compages)
			apps.push([path, app]);
		else
			apps.push(app);
	};

	/**
	 * Returns an express app that serves the site at the proper subdomain.
	 */
	self.serve = () =>
	{
		const Express = require('express');
		let app = Express();
		app.engine(p.renderer, renderer.serve());
		app.set('view engine', p.renderer);
		app.set('views', p.dirs.pages);

		app.disable('x-powered-by');

		apps.forEach(subapp =>
		{
			if (Array.isArray(subapp))
				app.use(subapp[0], subapp[1]);
			else
				app.use(subapp.urlPattern(), subapp.serve());
		});

		if (p.host)
			return VHost(p.host, (...args) => 
			{
				debug('served for vhost:', p.host);
				app(...args);
			});
		else
			return app;
	};

	/**
	 * Returns a gulp instance with tasks to build the site statically.
	 * If a gulp instance is passed, it will add the various build tasks to it.
	 * Returns the name of the task in gulp used to build this.
	 */
	self.build = (gulp, path, taskname, _renderer) =>
	{
		if (!gulp)
			gulp = require('gulp');

		//site doesn't pay attention to anyone trying to override its path.
		//Sites aren't transforms-- they are static sites. They actually live 
		//somewhere.
		path = p.pages;

		if (!taskname)
			taskname= 'build';

		taskname += `-${p.name}`;

		let tasks = apps.map(apparr =>
		{
			let subpath = path.posix.join(path, apparr[0]);
			let subapp  = apparr[1];

			return subapp.build(gulp, path, taskname, renderer);
		});
		gulp.task(taskname, tasks);
	};

	self.getHost = () => p.host;

	const apps     = [];
	const renderer = cpg.contrib[p.renderer]({id:p.id+'-renderer'});

	if (p && p.apps)
	{
		Object.keys(p.apps).forEach(k =>
		{
			let prms = p.apps[k];
			if (prms.disabled)
				return;
			//only compages apps can be set in compages.json.
			//fallbacks only exist for custom apps (obv)
			let subapp = cpg.contrib[prms.app](cpg, prms);
			self.use(subapp);
		});
	}

	return self;
}
